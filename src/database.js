const mongoose = require ('mongoose');

mongoose.connect('mongodb://localhost/notes-db-app',{
    userCreateIndex:true,
    useNewUrlParser:true,
    useFindAndModify:false
})
.then(db=>console.log('db is connected'))
.catch(error=> console.error(error));